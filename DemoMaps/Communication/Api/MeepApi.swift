//
//  MeepApi.swift
//  DemoMaps
//
//  Created by David Gutiérrez Galán on 24/11/2019.
//  Copyright © 2019 David Gutiérrez Galán. All rights reserved.
//

import Foundation


let BASE_URL: String = "https://apidev.meep.me/tripplan/api/v1/routers/lisboa"


public protocol MeepApiProtocol: class {
    func getMeepResources(lowerLeftLatLon: String, upperRightLatLon: String, completion: @escaping(Result<[Resource], GetResourcesError>)-> Void)
}

internal enum MeepCall: ApiMethod {
    case get_resources
    
    func getUrl() -> String {
        switch self {
        case .get_resources:
            return "\(BASE_URL)/resources"
        }
    }
    
    var httpMethod: String? {
        switch self {
        case .get_resources:
            return "GET"
        }
    }
    
}

public class MeepApi: MeepApiProtocol {
    
    private var requestSender: RequestSender
    
    public init(requestSender: RequestSender = RequestSender()) {
        self.requestSender = requestSender
    }
    
    public func getMeepResources(lowerLeftLatLon: String, upperRightLatLon: String, completion: @escaping(Result<[Resource], GetResourcesError>)-> Void){
        requestSender.sendRequest(method: MeepCall.get_resources, parameters: ["lowerLeftLatLon":lowerLeftLatLon,"upperRightLatLon":upperRightLatLon], token: nil, json: nil, completionHandler: {(data, response, error) in
            guard error == nil else {
                completion(.failure(GetResourcesError.connectionError))
                return
            }
            if let response = response as? HTTPURLResponse {
                let statusCode = response.statusCode
                if 200 ... 299 ~= statusCode {
                    let decoder = JSONDecoder()
                    if let data = data {
                        if let resourceResponse = try? decoder.decode(GetResourcesResponse.self, from: data) {
                            completion(.success(resourceResponse.resources))
                        } else {
                            completion(.failure(GetResourcesError.canNotProcessData))
                        }
                    } else {
                        completion(.failure(GetResourcesError.noDataAvailable))
                    }
                } else {
                    completion(.failure(GetResourcesError.httpError))
                }
            } else {
                completion(.failure(GetResourcesError.httpError))
            }
            
        })
    }
    
}
