//
//  RequestSender.swift
//  DemoMaps
//
//  Created by David Gutiérrez Galán on 24/11/2019.
//  Copyright © 2019 David Gutiérrez Galán. All rights reserved.
//

import Foundation

protocol ApiMethod {
    func getUrl() -> String
    var httpMethod: String? { get }
}

public class RequestSender {
    
    public init(){}
    
    func sendRequest(method: ApiMethod, parameters: [String: String]?, token: String?, json: [String: Any]?, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void){
        var jsonData: Data?
        if let json = json {
            jsonData = try? JSONSerialization.data(withJSONObject: json)
        }
        
        
        if var components = URLComponents(string: method.getUrl()) {
            if let parameters = parameters {
                components.queryItems = parameters.map { (key, value) in
                    URLQueryItem(name: key, value: value)
                }
                components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
            }
            if let url = components.url {
                var request = URLRequest(url: url)
                if let httpMethod = method.httpMethod {
                    request.httpMethod = httpMethod
                
                }
                if let token = token {
                    request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
                }
                
                if let jsonData = jsonData {
                    request.setValue("application/json", forHTTPHeaderField: "Accept")
                    request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                    request.httpBody = jsonData
                }
                let session = URLSession(configuration: .default)
                session.dataTask(with: request, completionHandler: completionHandler).resume()
            }
        }
    }
}
