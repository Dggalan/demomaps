//
//  GetResources.swift
//  DemoMaps
//
//  Created by David Gutiérrez Galán on 25/11/2019.
//  Copyright © 2019 David Gutiérrez Galán. All rights reserved.
//

import Foundation

public struct GetResourcesResponse: Codable {
    var resources: [Resource]
    
    public init(from decoder: Decoder) throws {
        resources = []
        let container = try decoder.singleValueContainer()
        if let _resources = try? container.decode([Resource].self){
            resources = _resources
        }
        
    }
}

public enum GetResourcesError: Error {
    case connectionError
    case noDataAvailable
    case canNotProcessData
    case httpError
}
