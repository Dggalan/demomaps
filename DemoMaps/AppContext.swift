//
//  AppContext.swift
//  DemoMaps
//
//  Created by David Gutiérrez Galán on 24/11/2019.
//  Copyright © 2019 David Gutiérrez Galán. All rights reserved.
//

import UIKit

protocol AppContextProtocol: class {
    
    var appCoordinator: AppCoordinator? { get set }
    var coordinatorFactory: CoordinatorFactoryProtocol { get set }
    var viewControllerFactory: ViewControllerFactoryProtocol { get set }
    var viewModelFactory: ViewModelFactoryProtocol { get set }
    var meepApi: MeepApiProtocol { get }
    
}

final class AppContext: AppContextProtocol {
    
    var appCoordinator: AppCoordinator?
    var viewControllerFactory: ViewControllerFactoryProtocol
    var viewModelFactory: ViewModelFactoryProtocol
    var coordinatorFactory: CoordinatorFactoryProtocol
    
    internal var meepApi: MeepApiProtocol
    
    let window: UIWindow!
    let alertWindow: UIWindow!
   

    init(window: UIWindow, alertWindow: UIWindow) {
        self.window = window
        self.alertWindow = alertWindow
        
        meepApi = MeepApi()
        viewControllerFactory = ViewControllerFactory()
        viewModelFactory = ViewModelFactory()
        coordinatorFactory = CoordinatorFactory()
        appCoordinator = coordinatorFactory.makeAppCoordinator(appContext: self, window: window)
        appCoordinator?.start(parentCoordinator: nil)
        
    }
}
