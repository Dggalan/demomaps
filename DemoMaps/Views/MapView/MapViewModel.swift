//
//  MapViewModel.swift
//  DemoMaps
//
//  Created by David Gutiérrez Galán on 24/11/2019.
//  Copyright (c) 2019 David Gutiérrez Galán. All rights reserved.
//

import CoreLocation
import GoogleMaps

// This class will comunicate with the ViewController in order to set the view and receive
// user interactions. It will get Data from local storage
// or API calls. It will comunicate with the Coordinator of the next module if it is necessary.
protocol MapViewModelProtocol: BaseViewModelProtocol {
    
    /**
     * Add here your methods for communication VIEW -> VIEWMODEL
     */
    var updateResources: (_ resources: [Resource], _ cleanMap: Bool) -> Void { get set }
    func onUserWillMove(gesture: Bool)
    func onRegionChanged(region: GMSVisibleRegion)
    
}

final class MapViewModel: BaseViewModel, MapViewModelProtocol {

    // MARK: - Properties
    
	var _delegateCoordinator: MapCoordinatorProtocol? {
        return delegateCoordinator as? MapCoordinatorProtocol
    }
    
    unowned let meepApi: MeepApiProtocol!
    
    var updateResources: ([Resource], Bool) -> Void = { _,_ in }
    
    internal var userIsMovingMap = true
    
    internal var resources: [Resource] = [Resource]() {
        didSet {
            // Update view
            self.updateResources(resources, userIsMovingMap)
        }
    }
	
    // MARK: - Object lifecycle
    init(delegateCoordinator: MapCoordinatorProtocol, meepApi: MeepApiProtocol) {
		self.meepApi = meepApi
        super.init(delegateCoordinator: delegateCoordinator)
	}
	
    deinit {
        debugPrint("Deinit Map")
    }
    
    override func onViewDidLoad() {
        super.onViewDidLoad()
    }
    
    // MARK: - Private functions
    
    func onUserWillMove(gesture: Bool){
        userIsMovingMap = gesture
    }
    
    // MARK: - MapViewModelProtocol
    
    func onRegionChanged(region: GMSVisibleRegion){
        if userIsMovingMap {
            let upperRight: CLLocationCoordinate2D = region.farRight
            let lowerLeft: CLLocationCoordinate2D = region.nearLeft
            
            meepApi.getMeepResources(lowerLeftLatLon: "\(lowerLeft.latitude),\(lowerLeft.longitude)", upperRightLatLon: "\(upperRight.latitude),\(upperRight.longitude)", completion: {
                [weak self] result in
                switch result {
                case .success(let resources):
                    debugPrint("Received \(resources.count) resources.")
                    self?.resources = resources
                case .failure(let error):
                    debugPrint("Error \(error).")
                    switch error {
                    case .connectionError:
                        self?.showAlert("Error de conexión", "Por favor, revise su conexión a internet.")
                    default:
                        self?.showAlert("Error en la llamada", "Se ha producido un error en la llamada.")
                    }
                    
                    
                }
            
                
            })
        }
    }

}
