//
//  MapViewController.swift
//  DemoMaps
//
//  Created by David Gutiérrez Galán on 24/11/2019.
//  Copyright (c) 2019 David Gutiérrez Galán. All rights reserved.
//

import UIKit
import GoogleMaps

// This class will controll the UI View. It will comunicate with the ViewModel in order to
// send the user interactions. This layer should keep as simple as possible. This layer should not
// have any logic, only getters and setters of the user information.
protocol MapViewControllerProtocol: BaseViewControllerProtocol {
    
}

final class MapViewController: BaseViewController, MapViewControllerProtocol {
    
    // MARK: - Properties
    var mapView: GMSMapView?
    
    var companiesImages = [Int:UIImage]()
	
	var _viewModel:  MapViewModelProtocol? {
        return viewModel as?  MapViewModelProtocol
    }
	
    // MARK: - Object lifecycle
    
    init(viewModel: MapCoordinator.ViewModelProtocol) {
        super.init(viewModel: viewModel, nibName: "MapView", bundle: Bundle(for: MapViewController.self))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    deinit {
        debugPrint("Deinit MapViewController")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
	
	override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configView()
    }
    
    // MARK: - Private functions
	
	fileprivate func configView() {
        title = "Map View"
        binding()
        loadMap()
	}
    
    fileprivate func loadMap(){
        let lowerLeftLat = 38.711046
        let lowerLeftLon = -9.160096
        let upperRightLat = 38.739429
        let upperRightLon = -9.137115
        
        let middleLat = lowerLeftLat + ((lowerLeftLat - upperRightLat) / 2)
        let middleLon = lowerLeftLon + ((lowerLeftLon - upperRightLon) / 2)
        
        debugPrint("middleLat: \(middleLat)")
        debugPrint("middleLon: \(middleLon)")
        
        let camera = GMSCameraPosition.camera(withLatitude: middleLat, longitude: middleLon, zoom: 12.0)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView
        mapView?.delegate = self
        if let mapView = mapView {
            _viewModel?.onRegionChanged(region: mapView.projection.visibleRegion())
        }
    }
    
    
    
    fileprivate func binding(){
        _viewModel?.updateResources = { [weak self] resources, cleanMap in
            DispatchQueue.main.async {
                if cleanMap {
                    self?.mapView?.clear()
                }
                
                resources.forEach({ resource in
                    if self?.companiesImages[resource.companyZoneId] == nil{
                        self?.companiesImages[resource.companyZoneId] = GMSMarker.markerImage(with: UIColor.random)
                    }
                    let marker = ResourceMarker(resource: resource, image: self?.companiesImages[resource.companyZoneId])
                    marker.map = self?.mapView
                })
 
            }
        }
        
    }
}





extension MapViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        _viewModel?.onUserWillMove(gesture: gesture)
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        _viewModel?.onRegionChanged(region: mapView.projection.visibleRegion())
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker:GMSMarker) -> Bool {
        return false
    }
    
    
    
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
      return nil
    }
    
}
