//
//  MapCoordinator.swift
//  DemoMaps
//
//  Created by David Gutiérrez Galán on 24/11/2019.
//  Copyright (c) 2019 David Gutiérrez Galán. All rights reserved.
//

import UIKit

// This class will construct and present the module.
protocol MapCoordinatorProtocol: BaseCoordinatorProtocol {
    var appContext: AppContextProtocol { get }
}

final class MapCoordinator: BaseCoordinator, MapCoordinatorProtocol {
    typealias ViewModelProtocol = MapViewModelProtocol
    typealias ViewControllerProtocol = MapViewControllerProtocol

    // MARK: - Properties
    unowned let window: UIWindow
    unowned let appContext: AppContextProtocol

    private var viewModel: ViewModelProtocol?
    private var viewController: ViewControllerProtocol?
    private var navigationController: UINavigationController?
    
    // MARK: - Object lifecycle
    internal init(appContext: AppContextProtocol, window: UIWindow) {
        self.appContext = appContext
        self.window = window
    }

    override func start(parentCoordinator: BaseCoordinatorProtocol? = nil) {
        super.start(parentCoordinator: parentCoordinator)
        viewModel = appContext.viewModelFactory.makeMapViewModel(delegateCoordinator: self, meepApi: appContext.meepApi)
        viewController = appContext.viewControllerFactory.makeMapViewController(viewModel: viewModel!)
        navigationController = UINavigationController(rootViewController: viewController!)
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }

    override func didFinish() {
        super.didFinish()
        viewController = nil
        viewModel = nil
    }


    deinit {
        debugPrint("Deinit Map")
    }

}

