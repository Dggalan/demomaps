//
//  BaseCoordinator.swift
//  DemoMaps
//
//  Created by David Gutiérrez Galán on 24/11/2019.
//  Copyright © 2019 David Gutiérrez Galán. All rights reserved.
//

import UIKit


public protocol BaseViewModelCoordinatorDelegate {

    func didFinish()

}

/// A coordinator that encapsulates navigation logic
public protocol BaseCoordinatorProtocol: class, BaseViewModelCoordinatorDelegate {

    /// Tells the coordinator to start its work, i.e. present a view controller.
    func start(parentCoordinator: BaseCoordinatorProtocol?)

    /// Marks the coordinator as finished.
    ///
    /// The coordinator will remove itself from its parent.
    func didFinish()

    /// Add a child coordinator
    func add(child: BaseCoordinatorProtocol)

    /// Remove a child coordinator
    ///
    /// Usually, you shouldn't need to call this method. Instead it is preferable
    /// to call the child coordinator's `didFinish` method.
    func remove(child: BaseCoordinatorProtocol)

    func removeAllChilds()

}

/// Base class for coordinators
open class BaseCoordinator: NSObject, BaseCoordinatorProtocol {

    var parentCoordinator: BaseCoordinatorProtocol?

    var childCoordinators: [BaseCoordinatorProtocol] = []

    open func start(parentCoordinator: BaseCoordinatorProtocol? = nil) {
        parentCoordinator?.add(child: self)
    }

    open func didFinish() {
        parentCoordinator?.remove(child: self)
    }

    public final func add(child: BaseCoordinatorProtocol) {
        childCoordinators.append(child)
        if let impl = child as? BaseCoordinator {
            impl.parentCoordinator = self
        }
    }

    public final func removeAllChilds() {
        childCoordinators.forEach({
            $0.didFinish()
        })
        childCoordinators.removeAll()
    }

    public final func remove(child: BaseCoordinatorProtocol) {
        let index = childCoordinators.firstIndex {
            $0 === child
        }

        if let index = index {
            childCoordinators.remove(at: index)
        }
    }

}

