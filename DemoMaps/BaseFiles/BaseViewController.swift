//
//  BaseViewController.swift
//  DemoMaps
//
//  Created by David Gutiérrez Galán on 24/11/2019.
//  Copyright © 2019 David Gutiérrez Galán. All rights reserved.
//
import UIKit
import SwifterSwift

public protocol BaseViewControllerProtocol: UIViewController {
    func reloadView()
    func didFinish()
}

public extension BaseViewControllerProtocol {
    func reloadView() { }
    func didFinish() { }
}


@IBDesignable
open class BaseViewController: UIViewController, BaseViewControllerProtocol {

    public var endEditingPressed: () -> Void = { }

    public var reloadView: () -> Void = { }

    public var didFinish: () -> Void = { }

    public var viewModel: BaseViewModelProtocol?


    fileprivate lazy var endEditingGestureRecognizer: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer(target: self, action: #selector(press(_:)))
        tap.cancelsTouchesInView = false
        return tap
    }()

    fileprivate lazy var endEditingNavigationBarGestureRecognizer: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer(target: self, action: #selector(press(_:)))
        tap.cancelsTouchesInView = false
        return tap
    }()

    // MARK: - Properties

    public init(viewModel: BaseViewModelProtocol, nibName: String? = nil, bundle: Bundle = Bundle.main) {
        super.init(nibName: nibName, bundle: bundle)
        self.viewModel = viewModel
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.viewModel = nil
    }

    open override func viewDidLoad() {
        super.viewDidLoad()
        mainBinding()
        viewModel?.onViewDidLoad()
    }

    // MARK: - Object lifecycle
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        baseConfigView()
        viewModel?.onViewWillAppear()
    }

    override open func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if isMovingFromParent || isBeingDismissed {
            viewModel?.didFinish()
        }
    }

    open override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    override open func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.view.transform = CGAffineTransform.init(translationX: 0, y: 0)
        self.view.layoutIfNeeded()
    }
    

    @objc private func press(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
        endEditingPressed()
    }

}

extension BaseViewController {

    private func mainBinding() {
        viewModel?.didFinish = { [weak self] in
            guard let `self` = self else { return }
            DispatchQueue.main.async {
                (self.viewModel as? BaseViewModel)?.delegateCoordinator?.didFinish()
                self.didFinish()
            }
        }
        
        (viewModel as? BaseViewModel)?.ShowSpinnerVC = { [weak self] timeOut in
            // Insert here your code to show the default spinner
        }
        viewModel?.hideSpinner = { [weak self] in
            // Insert here your code to hide the default spinner
        }
        
        viewModel?.reloadView = { [weak self] in
            guard let `self` = self else { return }
            DispatchQueue.main.async {
                self.reloadView()
            }
        }

        viewModel?.showActionSheet = { [weak self] title, message, cancelTitle, buttonTitles, ipadView, completion in
            guard let `self` = self else { return }
            DispatchQueue.main.async {
                let actionSheetVC = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
                #if os (iOS)
                    if UIDevice.current.userInterfaceIdiom == .pad {
                        actionSheetVC.modalPresentationStyle = UIModalPresentationStyle.popover
                        let pop = actionSheetVC.popoverPresentationController

                        if let ipadView = ipadView as? UIBarButtonItem {
                            pop?.barButtonItem = ipadView
                        } else if let ipadView = ipadView as? UIView {
                            pop?.sourceView = ipadView
                            pop?.sourceRect = ipadView.bounds
                        }
                    }
                #endif
                if let titles = buttonTitles {
                    for (index, title) in titles.enumerated() {
                        let action = UIAlertAction(title: title, style: .default, handler: { action -> Void in
                            if let completion = completion {
                                completion(index + 1)
                            }
                        })
                        actionSheetVC.addAction(action)
                    }
                }

                let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: { action -> Void in
                    if let completion = completion {
                        completion(0)
                    }
                })
                actionSheetVC.addAction(cancelAction)
                self.present(actionSheetVC, animated: true, completion: nil)
            }
        }

        viewModel?.showAlert = { [weak self] title, message in
            guard let `self` = self else { return }
            DispatchQueue.main.async {
                self.showAlert(title: title, message: message)
            }
        }

        didFinish = { [weak self] in
            guard let `self` = self else { return }
            
            (self.viewModel as? BaseViewModel)?.delegateCoordinator = nil
            self.viewModel = nil
        }

    }

    private func baseConfigView() {
        view.backgroundColor = UIColor.white
        //navigationController?.tintColor = UIColor.secondary ?? .black
    }

    public func autoHideKeyboard() {
        view.addGestureRecognizer(endEditingGestureRecognizer)
        navigationController?.navigationBar.addGestureRecognizer(endEditingNavigationBarGestureRecognizer)
    }
}


