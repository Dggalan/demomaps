//
//  BaseViewModel.swift
//  DemoMaps
//
//  Created by David Gutiérrez Galán on 24/11/2019.
//  Copyright © 2019 David Gutiérrez Galán. All rights reserved.
//

import UIKit

public protocol BaseViewModelProtocol: class {
    var didFinish: () -> Void { get set }
    var reloadView: () -> Void { get set }

    var hideSpinner: () -> Void { get set }
    
    var showActionSheet: (_ title: String, _ description: String, _ cancelTitle: String, _ buttonTitles: [String]?, _ ipadReferenceView: Any, _ completion: ((Int) -> Void)?) -> Void { get set }
    
    var showAlert: (_ title: String, _ description: String) -> Void { get set }

    func showSpinner (time: TimeInterval)
    func onViewDidLoad()
    func onViewWillAppear()

}


open class BaseViewModel: BaseViewModelProtocol {
    
    public var delegateCoordinator: BaseViewModelCoordinatorDelegate? {
        didSet {
            oldValue?.didFinish()
        }
    }

    internal var ShowSpinnerVC: (_ timeOut: TimeInterval) -> Void = { _ in }

    public var hideSpinner: () -> Void = { }

    public var didFinish: () -> Void = { }

    public var reloadView: () -> Void = { }
    
    public var showAlert: (_ title: String, _ description: String) -> Void = { _, _ in }
    
    public var showActionSheet: (_ title: String, _ description: String, _ cancelTitle: String, _ buttonTitles: [String]?,_ ipadReferenceView: Any, _ completion: ((Int) -> Void)?) -> Void = { _,_,_,_,_,_ in }

    public init(delegateCoordinator: BaseViewModelCoordinatorDelegate) {
        self.delegateCoordinator = delegateCoordinator
    }

    open func showSpinner (time: TimeInterval = 10) {
        self.ShowSpinnerVC(time)
    }

    open func onViewDidLoad() {

    }

    open func onViewWillAppear() {

    }

}
