//
//  AppCoordinator.swift
//  DemoMaps
//
//  Created by David Gutiérrez Galán on 24/11/2019.
//  Copyright © 2019 David Gutiérrez Galán. All rights reserved.
//

import UIKit


final class AppCoordinator: BaseCoordinator {
    let appContext: AppContextProtocol
    let window: UIWindow!
    
    var mapCoordinator: MapCoordinatorProtocol!
    
    init(withWindows window: UIWindow, appContext: AppContextProtocol) {
        self.window = window
        self.appContext = appContext
    }

    override func start(parentCoordinator: BaseCoordinatorProtocol? = nil) {
        super.start(parentCoordinator: parentCoordinator)
        loadMapView()
    }
    
    fileprivate func loadMapView() {
        mapCoordinator = appContext.coordinatorFactory.makeMapCoordinator(appContext: appContext, window: window)
        mapCoordinator.start(parentCoordinator: self)
    }



}
