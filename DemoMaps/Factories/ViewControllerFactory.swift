//
//  ViewControllerFactory.swift
//  DemoMaps
//
//  Created by David Gutiérrez Galán on 24/11/2019.
//  Copyright © 2019 David Gutiérrez Galán. All rights reserved.
//

import Foundation

protocol ViewControllerFactoryProtocol {
    func makeMapViewController(viewModel: MapViewModelProtocol) -> MapViewControllerProtocol

}

struct ViewControllerFactory: ViewControllerFactoryProtocol {
    func makeMapViewController(viewModel: MapViewModelProtocol) -> MapViewControllerProtocol {
        return MapViewController.init(viewModel: viewModel)
    }
    

}
