//
//  ViewModelFactory.swift
//  DemoMaps
//
//  Created by David Gutiérrez Galán on 24/11/2019.
//  Copyright © 2019 David Gutiérrez Galán. All rights reserved.
//

import Foundation

protocol ViewModelFactoryProtocol {
    func makeMapViewModel(delegateCoordinator: MapCoordinator, meepApi: MeepApiProtocol) -> MapViewModelProtocol
}

struct ViewModelFactory: ViewModelFactoryProtocol {
    func makeMapViewModel(delegateCoordinator: MapCoordinator, meepApi: MeepApiProtocol) -> MapViewModelProtocol {
        return MapViewModel.init(delegateCoordinator: delegateCoordinator, meepApi: meepApi)
    }
}
