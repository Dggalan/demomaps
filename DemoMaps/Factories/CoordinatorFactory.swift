//
//  CoordinatorFactory.swift
//  DemoMaps
//
//  Created by David Gutiérrez Galán on 24/11/2019.
//  Copyright © 2019 David Gutiérrez Galán. All rights reserved.
//
import UIKit

protocol CoordinatorFactoryProtocol {
    func makeAppCoordinator(appContext: AppContextProtocol, window: UIWindow) -> AppCoordinator
    func makeMapCoordinator(appContext: AppContextProtocol, window: UIWindow) -> MapCoordinatorProtocol

}

struct CoordinatorFactory: CoordinatorFactoryProtocol {

    func makeAppCoordinator(appContext: AppContextProtocol, window: UIWindow) -> AppCoordinator {
        return AppCoordinator.init(withWindows: window, appContext: appContext)
    }
    func makeMapCoordinator(appContext: AppContextProtocol, window: UIWindow) -> MapCoordinatorProtocol {
        return MapCoordinator.init(appContext: appContext, window: window)
    }
}

