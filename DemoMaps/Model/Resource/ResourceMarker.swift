//
//  ResourceMarker.swift
//  DemoMaps
//
//  Created by David Gutiérrez Galán on 25/11/2019.
//  Copyright © 2019 David Gutiérrez Galán. All rights reserved.
//

import GoogleMaps
import UIKit

public class ResourceMarker: GMSMarker {

    let resource: Resource
  
    init(resource: Resource, image: UIImage?) {
      self.resource = resource
      super.init()
      
      position = CLLocationCoordinate2DMake(resource.y, resource.x)
      title = resource.name
      snippet = "Company Zone: \(resource.companyZoneId)"
      icon = image
      groundAnchor = CGPoint(x: 0.5, y: 1)
      appearAnimation = .pop
    }
    
}
