//
//  Resource.swift
//  DemoMaps
//
//  Created by David Gutiérrez Galán on 25/11/2019.
//  Copyright © 2019 David Gutiérrez Galán. All rights reserved.
//

import Foundation
import CoreLocation


public struct Resource: Codable {
    var id: String
    var name: String
    var companyZoneId: Int
    
    var x: Double
    var y: Double
    
    var lon: Double?
    var lat: Double?
 
}
