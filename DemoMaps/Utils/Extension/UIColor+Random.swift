//
//  UIColor+Random.swift
//  DemoMaps
//
//  Created by David Gutiérrez Galán on 25/11/2019.
//  Copyright © 2019 David Gutiérrez Galán. All rights reserved.
//

import UIKit

public extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}
